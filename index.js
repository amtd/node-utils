const deploy = require('./deploy.js')
const versions = require('./versions.js')

exports.versions = versions
exports.deploy = deploy
