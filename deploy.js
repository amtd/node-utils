exports.deploy = function(stage) {
  const os = require('os')
  const fs = require('fs')
  const exec = require('child_process').execSync

  // NOTE: npm install rsyncwrapper
  const rsync = require('rsyncwrapper')

  // NOTE: npm install request
  const request = require('request')

  // NOTE: Loads package.json relative to running process
  const package = require(process.cwd() + '/package.json')
  /* EXAMPLE package.json:
  {
    "name": "app",
    "config": {
      "deploy": {
        "stages": {
          "test": {
            "address": "somewhere.com",
            "port": "22",
            "user": "appuser",
            "branch": "gitbranch",
            "deploy_to": "/srv/app",
            "public_url": "https://app.public",
            "slack_url": "https://hooks.slack.com/services/abacabadaba/ubadubaluba"
          }
        }
      }
    }
  }
  */

  const process_start_at = new Date()
  const git_revision = exec('git rev-parse --short HEAD').toString().trim()
  const git_branch = exec('git rev-parse --abbrev-ref HEAD').toString().trim()
  const deployer = `${os.userInfo().username}@${os.hostname()}`

  // const stage = process.argv[2]
  const config = package.config.deploy.stages[stage]
  if (!config) {
    console.error(`\x1b[31mEnvironment ${stage} not found!`)
    return 1
  }

  if (config.branch && config.branch != git_branch) {
    console.error(`Wrong current branch, got ${git_branch} must be ${config.branch}!`)
    return 3
  }

  let build_path = package.config.deploy.build_path
  if (!build_path) {
    console.error('Build path is empty!')
    return 2
  }
  if (!build_path.match(/^.+\/\.?$/)) {
    build_path += '/'
  }

  const valid_masks = [
    /^index\.html$/,
    /^assets$/,
    /^[0-9]+\.[a-z0-9]+\.js$/,
    /^styles\.[0-9a-z]+\.css/,
    /^polyfills\.[0-9a-z]+\.js$/,
    /^runtime\.[0-9a-z]+\.js$/,
    /^common\.[0-9a-z]+\.js$/,
    /^.+\.woff2?$/
  ]
  const build_files = fs.readdirSync(build_path)
  const valid_build_files = build_files.filter(name => {
    return !!valid_masks.find(re => name.match(re))
  })

  if (valid_build_files.length / build_files.length < 0.75) {
    console.error(`Build path ${build_path} maybe wrong.`)
    return 3
  }

  function notify_slack(error = undefined) {
    const process_finish_at = new Date()
    const process_run_delta = (process_finish_at - process_start_at) / 1000

    const options = {
      method: 'post',
      body: {
        text: '',
        attachments: [{
          title: `${package.name} ${stage}`,
          title_link: config.public_url,
          text: '',
          color: '',
          author_name: 'deploy.js',
          footer: '',
          footer_icon: `${config.public_url}/assets/images/logo.png`,
          ts: Math.floor(process_finish_at / 1000),
          fields: [{
              title: 'Stage',
              value: stage,
              short: true
            },
            {
              title: 'Version',
              value: package.version,
              short: true
            },
            {
              title: 'Branch',
              value: git_branch,
              short: true
            },
            {
              title: 'Revision',
              value: git_revision,
              short: true
            }
          ]
        }]
      },
      json: true,
      url: config.slack_url,
      headers: {}
    }

    if (error) {
      options.body.attachments[0].text = `Deploy by ${deployer}: *failed* in ${process_run_delta}s.`
      options.body.attachments[0].color = 'danger'
    } else {
      options.body.attachments[0].text = `Deploy by ${deployer}: _successful_ in ${process_run_delta}s.`
      options.body.attachments[0].color = 'good'
    }

    request(options, function (err, res, body) {
      if (err) {
        console.log('Slack error:', err)
        return
      }
    });
  }

  rsync({
      src: build_path,
      dest: `${config.user}@${config.address}:${config.deploy_to}`,
      port: `${config.port}`,
      ssh: true,
      recursive: true,
      delete: true,
      args: ['-acvzh']
    },
    function (error, stdout, stderr, cmd) {
      const process_finish_at = new Date()

      console.log(cmd)
      if (error) {
        console.log(error.message)
        notify_slack(error)
      } else {
        console.log(stdout)
        notify_slack()
      }

      console.log('Total execution time: %dms', process_finish_at - process_start_at)
    }
  )
}